import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()

export class CartService {

    public cartItem: Array<any> = [];
    private _cartSubject = new Subject<any>();
    cart = this._cartSubject.asObservable();

    constructor() {
    }

    removeProduct(product: object){
    }

    addProduct(product: object, quantity: number) {
      if (this.cartItem == null || this.cartItem == []) {
        this.cartItem = [{"product" : product, "quantity" : quantity}];
        this._cartSubject.next([{"product" : product, "quantity" : quantity}]);
      } else {
        this.cartItem.push({"product" : product, "quantity" : quantity});
        this._cartSubject.next(this.cartItem);
      }
      localStorage.setItem('cart',JSON.stringify(this.cartItem));
    }

    addQuantity(product: number, quantity: number) {
      this.cartItem[product]["quantity"] += quantity;
      if(this.cartItem[product]["quantity"] == 0) {
        this.cartItem.splice(product, 1);
      }
      this._cartSubject.next(this.cartItem);
      localStorage.setItem('cart',JSON.stringify(this.cartItem));
    }

    getProducts() {
      return this.cartItem;
    }

    resetCart() {
      this.cartItem = [];
      this._cartSubject.next(this.cartItem);
    }

    fireInit() {
        var local = JSON.parse(localStorage.getItem('cart'));
        this.cartItem = local;
        this._cartSubject.next(this.cartItem);
    }
}
