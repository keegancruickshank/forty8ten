import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-phantom-boards',
  templateUrl: './phantom-boards.component.html',
  styleUrls: ['./phantom-boards.component.scss']
})
export class PhantomBoardsComponent implements OnInit {

  loading: boolean = true;
  apparelData: [any];

  constructor(database: AngularFireDatabase) {
    database.object("/shop/boards/phantom").snapshotChanges().subscribe(snapshot => {
      this.loading = false;
      var data = snapshot.payload.val();
      this.apparelData = data;
    })
  }

  ngOnInit() {
  }

  cancelLoader(i) {
    var id = "loader" + i;
    document.getElementById(id).style.display = "none";
  }

}
