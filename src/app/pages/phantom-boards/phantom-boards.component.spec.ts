import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhantomBoardsComponent } from './phantom-boards.component';

describe('PhantomBoardsComponent', () => {
  let component: PhantomBoardsComponent;
  let fixture: ComponentFixture<PhantomBoardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhantomBoardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhantomBoardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
