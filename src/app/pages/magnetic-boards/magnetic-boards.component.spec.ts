import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagneticBoardsComponent } from './magnetic-boards.component';

describe('MagneticBoardsComponent', () => {
  let component: MagneticBoardsComponent;
  let fixture: ComponentFixture<MagneticBoardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MagneticBoardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagneticBoardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
