import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-magnetic-boards',
  templateUrl: './magnetic-boards.component.html',
  styleUrls: ['./magnetic-boards.component.scss']
})
export class MagneticBoardsComponent implements OnInit {

  loading: boolean = true;
  apparelData: [any];

  constructor(database: AngularFireDatabase) {
    database.object("/shop/boards/magnetic").snapshotChanges().subscribe(snapshot => {
      this.loading = false;
      var data = snapshot.payload.val();
      this.apparelData = data;
    })
  }

  ngOnInit() {
  }

  cancelLoader(i) {
    var id = "loader" + i;
    document.getElementById(id).style.display = "none";
  }

}
