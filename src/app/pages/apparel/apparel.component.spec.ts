import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AparelComponent } from './aparel.component';

describe('AparelComponent', () => {
  let component: AparelComponent;
  let fixture: ComponentFixture<AparelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AparelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AparelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
