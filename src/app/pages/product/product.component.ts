import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  loading: boolean = true;
  public product: any;

  constructor(private route: ActivatedRoute, database: AngularFireDatabase, private router: Router, private cart: CartService) {
    const id = route.snapshot.paramMap.get('id');
    database.object("/shop").snapshotChanges().subscribe(snapshot => {
      var data = snapshot.payload.val();
      let boards = data["boards"];
      let apparel = data["apparel"];
      for(var item in apparel) {
        var currentItem = apparel[item];
        if (currentItem["name"] == id) {
          this.product = currentItem;
        }
      }
      for(var boardStyle in boards) {
        var currentStyle = boards[boardStyle]
        for(var item in currentStyle) {
          var currentItem = currentStyle[item];
          if (currentItem["name"] == id) {
            this.product = currentItem;
          }
        }
      }

    this.loading = false;
    })
  }

  addToCart(product, number) {
    this.cart.addProduct(product, number);
  }

  ngOnInit() {
  }


}
