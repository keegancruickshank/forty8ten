import { Component, OnInit } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { RouterModule, Routes, ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFireStorage } from 'angularfire2/storage';

@Component({
  selector: 'app-customiser',
  templateUrl: './customiser.component.html',
  styleUrls: ['./customiser.component.scss']
})
export class CustomiserComponent implements OnInit {

  //Database Customiser Data
  private data;

  dataLoaded = false;

  //Current Tab (Construction, Length, Etc.)
  area = 0;

  //Shape Data
  shapesSorted = [];
  shapesUnsorted = [];
  selectedShape: any;

  //Topsheet Data
  topsheets = {};
  topsheetsSorted = [];
  topsheetsUnsorted = [];
  selectedTopsheet: any;

  //Base Data
  bases = {};
  basesSorted = [];
  basesUnsorted = [];
  selectedBase: any;

  //Construction Data
  constructionsSorted = [];
  constructionsUnsorted = [];
  selectedConstruction: any;

  //Topsheet Data
  baseTekUnsorted = [];
  selectedBaseTek: any;

  //Lengths Data
  lengthsSorted = [];
  lengthsUnsorted = [];
  selectedLength: any;

  //Sidewall Data
  sidewalls = {};
  sidewallsSorted = [];
  sidewallsUnsorted = [];
  selectedSidewall: any;

  totalCost = 0;

  //Loader
  loading = true;

  leftImageLoading = true;
  rightImageLoading = true;

  selectedShapeLoader = "../../../assets/magnetic.png";
  selectedShapeName = "magnetic";

  topsheetsLoaded = [false, false, false, false, false, false];
  topsheetsLoadedMobile = [];
  basesLoaded = [false, false, false, false, false, false];
  basesLoadedMobile = [];

  constructor(private cartService: CartService, private router: Router, private database: AngularFireDatabase, private storage: AngularFireStorage) {
    database.object("/shop/customiser").snapshotChanges().subscribe(snapshot => {
      this.loading = false;
      let data = snapshot.payload.val();
      this.data = data
      this.initiliseBuilder(data);
      this.dataLoaded = true;
      this.calculateTotal();
    })
  }

  initiliseBuilder(data) {
    // -------- Shape Sorting -------- //
    this.shapesUnsorted = data["shapes"];
    var tempArray = [];
    var length = this.shapesUnsorted.length;
    for(var x = 0; x < length; x++) {
      if (x == 0){
        this.selectedShape = this.shapesUnsorted[x];
        this.shapesSorted.push(this.shapesUnsorted[x]);
      }else if(x<4) {
        this.shapesSorted.push(this.shapesUnsorted[x]);
      } else {
        tempArray.push(this.shapesUnsorted[x]);
      }
    }

    this.shapesUnsorted = tempArray;
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      classes[i].className = "item-image animated fadeOutRight"
    }
    // -------- END Shape Sorting -------- //


    // -------- Topsheets Sorting -------- //
    this.topsheetsLoadedMobile = [];
    this.topsheetsUnsorted = data["topsheets"];
    this.topsheets = data["topsheets"];
    var tempArray = [];
    var length = this.topsheetsUnsorted.length;
    for(var x = 0; x < length; x++) {
      this.topsheetsLoadedMobile.push(false);
      if (x == 0){
        this.selectedTopsheet = this.topsheetsUnsorted[x];
        this.topsheetsSorted.push(this.topsheetsUnsorted[x]);
      }else if(x<6) {
        this.topsheetsSorted.push(this.topsheetsUnsorted[x]);
      } else {
        tempArray.push(this.topsheetsUnsorted[x]);
      }
    }
    this.topsheetsUnsorted = tempArray;
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      classes[i].className = "item-image animated fadeOutRight"
    }
    // -------- END Topsheets Sorting -------- //


    // -------- Base Sorting -------- //
    this.basesLoadedMobile = [];
    this.basesUnsorted = data["bases"];
    this.bases = data["bases"];
    var tempArray = [];
    var length = this.basesUnsorted.length;
    for(var x = 0; x < length; x++) {
      this.basesLoadedMobile.push(false);
      if (x == 0){
        this.selectedBase = this.basesUnsorted[x];
        this.basesSorted.push(this.basesUnsorted[x]);
      }else if(x<6) {
        this.basesSorted.push(this.basesUnsorted[x]);
      } else {
        tempArray.push(this.basesUnsorted[x]);
      }
    }
    this.basesUnsorted = tempArray;
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      classes[i].className = "item-image animated fadeOutRight"
    }
    // -------- END Base Sorting -------- //

    // -------- Length Sorting -------- //
    this.lengthsUnsorted = this.selectedShape["sizes"];
    this.selectedLength = this.selectedShape["sizes"][0];
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      classes[i].className = "item-image animated fadeOutRight"
    }
    // -------- END Length Sorting -------- //



    // -------- Construction Sorting -------- //
    this.constructionsSorted = []
    var list = this.selectedShape["constructions"];
    var cons = data["constructions"];
    for(var i = 0; i<list.length; i++) {
      for(var j = 0; j<cons.length; j++) {
        if(list[i] == cons[j]["name"]){
          this.constructionsSorted.push(cons[j]);
        }
      }
    }
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      classes[i].className = "item-image animated fadeOutRight"
    }

    this.selectedConstruction = this.constructionsSorted[0];
    this.checkConstruction();
    // -------- END Construction Sorting -------- //

    // -------- Sidewall Sorting -------- //
    this.sidewallsUnsorted = data["sidewalls"];
    this.sidewalls = data["sidewalls"];
    var tempArray = [];
    var length = this.sidewallsUnsorted.length;
    for(var x = 0; x < length; x++) {
      if (x == 0){
        this.selectedSidewall = this.sidewallsUnsorted[x];
        this.sidewallsSorted.push(this.sidewallsUnsorted[x]);
      }else if(x<8) {
        this.sidewallsSorted.push(this.sidewallsUnsorted[x]);
      } else {
        tempArray.push(this.sidewallsUnsorted[x]);
      }
    }

    this.sidewallsUnsorted = tempArray;
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      classes[i].className = "item-image animated fadeOutRight"
    }
    // -------- END Sidewall Sorting -------- //
  }

  checkConstruction() {
    this.baseTekUnsorted = this.selectedConstruction["base_tek"];
    if(this.baseTekUnsorted != undefined) {
      this.selectedBaseTek = this.baseTekUnsorted[0];
    }
  }

  reRun() {
    let data = this.data;
    // -------- Length Sorting -------- //
    this.lengthsUnsorted = this.selectedShape["sizes"];
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      classes[i].className = "item-image animated fadeOutRight"
    }
    // -------- END Length Sorting -------- //
  }

  ngOnInit() {}

  addToCart() {
    this.calculateTotal();
    var board = {
      type: "custom",
      shape: this.selectedShape,
      length: this.selectedLength,
      construction: this.selectedConstruction,
      topsheet: this.selectedTopsheet,
      base: this.selectedBase,
      sidewall: this.selectedSidewall,
      baseTek: this.selectedBaseTek,
      total: this.totalCost
    }
    this.cartService.addProduct(board, 1);
    this.router.navigate(['cart']);
  }

  nextArea() {
    if(this.area == 5) {
      this.addToCart();
    } else {
      this.area += 1;
    }
  }

  previousArea() {
    this.area -= 1;
  }

  getArea(number) {
    if(number == this.area) {
      return "menu-item-customiser active";
    } else {
      return "menu-item-customiser";
    }
  }

  getSelectedTopsheets(topsheet): Array<any> {
    if(topsheet == this.selectedTopsheet){return ['item selected', 'inline']}else{return ['item', 'none']}
  }

  previousTopsheet() {
    var that = this;
    this.topsheetsLoaded[0] = false;
    that.topsheetsUnsorted.push(that.topsheetsSorted.pop());
    that.topsheetsSorted.unshift(that.topsheetsUnsorted.shift());
  }

  nextTopsheet () {
    var that = this;
    this.topsheetsLoaded[this.topsheetsLoaded.length - 1] = false;
    that.topsheetsUnsorted.unshift(that.topsheetsSorted.shift());
    that.topsheetsSorted.push(that.topsheetsUnsorted.pop());
  }

  getSelectedBase(base): Array<any> {
    if(base == this.selectedBase){return ['item selected', 'inline']}else{return ['item', 'none']}
  }

  getSelectedBaseTek(baseTek): Array<any> {
    if(baseTek == this.selectedBaseTek){return ['item selected', 'inline']}else{return ['item', 'none']}
  }

  previousBase() {
    var that = this;
    this.basesLoaded[0] = false;
    that.basesUnsorted.push(that.basesSorted.pop());
    that.basesSorted.unshift(that.basesUnsorted.shift());
  }

  nextBase () {
    var that = this;
    this.basesLoaded[this.basesLoaded.length - 1] = false;
    that.basesUnsorted.unshift(that.basesSorted.shift());
    that.basesSorted.push(that.basesUnsorted.pop());
  }

  getSelectedConstruction(base): Array<any> {
    if(base == this.selectedConstruction){return ['item selected', 'inline']}else{return ['item', 'none']}
  }

  getSelectedLength(length): Array<any> {
    if(length == this.selectedLength){return ['item selected', 'inline']}else{return ['item', 'none']}
  }

  getSelectedShape(shape): Array<any> {
    if(shape == this.selectedShape){return ['item selected', 'inline']}else{return ['item', 'none']}
  }

  getSelectedSidewall(sidewall): Array<any> {
    if(sidewall == this.selectedSidewall){return ['item selected', 'inline']}else{return ['item', 'none']}
  }

  previousSidewall() {
    var that = this;
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      if(i == (classes.length-1)) {
        classes[i].className = "item-image animated fadeOutUp"
      } else {
        classes[i].className = "item-image animated fadeOutRight"
      }
    }
    setTimeout(function() {
      that.sidewallsUnsorted.push(that.sidewallsSorted.pop());
      that.sidewallsSorted.unshift(that.sidewallsUnsorted.shift());
      classes = document.getElementsByClassName("item-image");
      for(var i = 0; i < classes.length; i++) {
        classes[i].className = "item-image animated fadeInLeft"
      }
    }, 250);
  }

  nextSidewall () {
    var that = this;
    var classes = document.getElementsByClassName("item-image");
    for(var i = 0; i < classes.length; i++) {
      if(i == 0) {
        classes[i].className = "item-image animated fadeOutUp"
      } else {
        classes[i].className = "item-image animated fadeOutLeft"
      }
    }
    setTimeout(function() {
      that.sidewallsUnsorted.unshift(that.sidewallsSorted.shift());
      that.sidewallsSorted.push(that.sidewallsUnsorted.pop());
      classes = document.getElementsByClassName("item-image");
      for(var i = 0; i < classes.length; i++) {
        classes[i].className = "item-image animated fadeInRight"
      }
    }, 250);
  }

  cancelLoader(i) {
    var id = "loader" + i;
    document.getElementById(id).style.display = "none";
  }

  calculateTotal() {
    if(this.selectedLength != [] && this.selectedConstruction != [] && this.selectedBase != [] && this.selectedTopsheet != [] && this.selectedShape != [] && this.selectedSidewall != []){
      this.totalCost = this.selectedLength["base_price"] + this.selectedConstruction["base_price"] + this.selectedBase["base_price"] + this.selectedTopsheet["base_price"] + this.selectedShape["base_price"] + this.selectedSidewall["base_price"];
    }
  }

  setLeftImageLoading() {
    this.leftImageLoading = true;
    document.getElementById("loader999").style.display = "block";
  }

  setRightImageLoading() {
    this.rightImageLoading = true;
    document.getElementById("loader998").style.display = "block";
  }

}
