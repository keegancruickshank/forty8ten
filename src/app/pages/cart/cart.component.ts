import { Component, OnInit } from '@angular/core';
import { CartService } from '../../services/cart.service';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import { RouterModule, Routes, ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public cartData: Array<object>;
  public totalCartPrice: number = 0;

  constructor(private route: ActivatedRoute, private router: Router, private cartService: CartService) {
    this.cartData = cartService.getProducts();
    this.calcTotal();
  }

  addOne(index: number) {
    this.cartService.addQuantity(index, 1);
    this.cartData = this.cartService.getProducts();
    this.calcTotal();
    document.getElementById("paypal-button").innerHTML = "";
    this.paypalInit();
  }

  takeOne(index: number) {
    this.cartService.addQuantity(index, -1);
    this.cartData = this.cartService.getProducts();
    this.calcTotal();
    document.getElementById("paypal-button").innerHTML = "";
    this.paypalInit();
  }

  calcTotal() {
    this.totalCartPrice = 0;
    if(this.cartData != null) {
      for(var i = 0; i < this.cartData.length; i++) {
        if(this.cartData[i]["product"]["type"] == "custom") {
          var price = this.cartData[i]["product"]["price"];
          var shape = this.cartData[i]["product"]["shape"]["base_price"];
          var length = this.cartData[i]["product"]["length"]["base_price"];
          var construction = this.cartData[i]["product"]["construction"]["base_price"];
          var topsheet = this.cartData[i]["product"]["topsheet"]["base_price"];
          var base = this.cartData[i]["product"]["base"]["base_price"];
          var sidewall = this.cartData[i]["product"]["sidewall"]["base_price"];
          var total = shape + length + construction + topsheet + base + sidewall;
          var quantity = this.cartData[i]["quantity"];
          this.totalCartPrice += (total*quantity);
        } else {
          var price = this.cartData[i]["product"]["price"];
          var quantity = this.cartData[i]["quantity"];
          this.totalCartPrice += (price*quantity);
        }
      }
    }
  }

  ngOnInit() {
    this.calcTotal();
    document.getElementById("paypal-button").innerHTML = "";
    this.paypalInit();
  }

  paypalInit() {
    var that = this
    var price = this.totalCartPrice;
    paypal.Button.render({

       env: 'sandbox', // Or 'sandbox'

       client: {
           sandbox:    'Abl1DXyvnaapmiV8Ee8Bk3Gl-zvs5AEVsU22GcnMi37pHa1R5b-3Q6ffH2mCDe0bJUpTdM6kWDH3jU5y',
           production: 'xxxxxxxxx'
       },

       style: {
        size: 'medium',
        color: 'black',
        shape: 'rect',
        label: 'checkout'
      },

       commit: true,

       payment: function(data, actions) {
           return actions.payment.create({
               payment: {
                   transactions: [
                       {
                           amount: { total: price, currency: 'AUD' }
                       }
                   ]
               }
           });
       },

       onAuthorize: function(data, actions) {
          return actions.payment.execute().then(function(payment) {
            that.cartService.resetCart();
            that.router.navigateByData({

               url: ["/purchase"],
               data: [payment, that.cartData],
             });
          });
       }

   }, '#paypal-button');
  }


}
