import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit {

  user: any;
  purchase: any;
  date = new Date(Date.now()).toLocaleString();

  constructor(private router: Router) {}

  ngOnInit() {
    // Redirect home if no data passed through
    var data = this.router.getNavigatedData();
    console.log(data)
    if(data == undefined) {
      this.router.navigate(['home']);
    } else {
      this.purchase = data[0];
      this.user = data[1];
    }
  }

}
