import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomeComponent } from './pages/home/home.component';
import { ApparelComponent } from './pages/apparel/apparel.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { PhantomBoardsComponent } from './pages/phantom-boards/phantom-boards.component';
import { MagneticBoardsComponent } from './pages/magnetic-boards/magnetic-boards.component';
import { CustomiserComponent } from './pages/customiser/customiser.component';
import { ShippingInfoComponent } from './pages/shipping-info/shipping-info.component';
import { ReturnsAndRefundsComponent } from './pages/returns-and-refunds/returns-and-refunds.component';
import { ProductComponent } from './pages/product/product.component';
import { PurchaseComponent } from './pages/purchase/purchase.component';
import "angular2-navigate-with-data";
import { CartComponent } from './pages/cart/cart.component';
import { CartService } from './services/cart.service';
import { LazyLoadImageModule } from 'ng-lazyload-image';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'apparel', component: ApparelComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'magnetic-boards', component: MagneticBoardsComponent },
  { path: 'phantom-boards', component: PhantomBoardsComponent },
  { path: 'customiser', component: CustomiserComponent },
  { path: 'shipping-info', component: ShippingInfoComponent },
  { path: 'returns-and-refunds', component: ReturnsAndRefundsComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'purchase', component: PurchaseComponent },
  { path: 'cart', component: CartComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    ApparelComponent,
    AboutUsComponent,
    ContactUsComponent,
    PhantomBoardsComponent,
    MagneticBoardsComponent,
    CustomiserComponent,
    ShippingInfoComponent,
    ReturnsAndRefundsComponent,
    ProductComponent,
    PurchaseComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LazyLoadImageModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app needed for everything
    AngularFireDatabaseModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule // imports firebase/storage only needed for storage features
  ],
  providers: [CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
