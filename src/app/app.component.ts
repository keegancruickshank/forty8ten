import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { RouterModule, Routes } from '@angular/router';
import { CartService } from './services/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  cartCount: number = 0;
  countClass: string = "count";
  loading: boolean = true;
  mobileMenuToggled = false;

  constructor(database: AngularFireDatabase, private cartService: CartService) {

  }

  toggleMenu() {
    this.mobileMenuToggled = !this.mobileMenuToggled;
  }

  submitTestEmail() {
    var data = new FormData();
    data.append('first_name', 'Jane');
    data.append('last_name', 'Mitchell');
    data.append('address', '123 Example Address');
    data.append('phone', '0408443641');
    data.append('email', 'jane@mitchellcreative.com.au');
    data.append('shape', 'Square');
    data.append('length', '142cm');
    data.append('topsheet', 'Pez Orange');
    data.append('base', 'Forty8ten Logo');
    data.append('sidewall', 'Neon Yellow');
    data.append('price', '598');

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:8888/forty8ten/order-made.php', true);
    xhr.onload = function (response) {
        // do something to response
        console.log(response);
    };
    xhr.send(data);
  }

  closeMenu() {
    this.mobileMenuToggled = false;
  }

  ngOnInit() {
    var that = this;
    this.cartService.cart.subscribe( data => {
      if(data != null) {
        this.cartCount = data.length;
        if(this.cartCount > 0) {
          this.countClass = "count animated zoomInUp";
          setTimeout(function() {
            that.countClass = "count";
          }, 2000)
        }
      }
    })
    this.cartService.fireInit();
  }
}
