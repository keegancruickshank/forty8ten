# Forty8ten

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` in a terminal for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. Run with `-prod` flag to develop with production errors.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Mitchell Creative

Developed by Keegan Cruickshank while employed by Mitchell Creative. For use of any code embed in this project, Please contact Mitchell Creative, https://mitchellcreative.com.au.
